# 09jul21 Software Lab. Alexander Burger

(allowed NIL
   "@lib.css" "!person" "!treeReport" "!contemporaries" )

(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l" "@lib/svg.l")

(symbols 'family 'svg 'pico)

### DB ###
(class +Person +Entity)
(rel nm (+Need +Sn +Idx +String))      # Name
(rel pa (+Joint) kids (+Man))          # Father
(rel ma (+Joint) kids (+Woman))        # Mother
(rel mate (+Joint) mate (+Person))     # Partner
(rel job (+Ref +String))               # Occupation
(rel dat (+Ref +Date))                 # born
(rel fin (+Ref +Date))                 # died
(rel txt (+String))                    # Info

(dm url> (Tab)
   (list "!person" '*ID This) )


(class +Man +Person)
(rel kids (+List +Joint) pa (+Person)) # Children

(class +Woman +Person)
(rel kids (+List +Joint) ma (+Person)) # Children

(dbs
   (0)                                 # @:64
   (2 +Person)                         # A:256
   (3 (+Person nm))                    # B:512
   (3 (+Person job dat fin)) )         # C:512



# Tree display of a person's descendants
(de treeReport (This)
   (html 0 "Family Tree View" "@lib.css" NIL
      (<h3> NIL "Family Tree View")
      (<ul> NIL
         (recur (This)
            (when (try 'url> This 1)
               (<li> NIL
                  (<href> (: nm) (mkUrl @))
                  (when (try 'url> (: mate) 1)
                     (prin " -- ")
                     (<href> (: mate nm) (mkUrl @)) ) )
               (when (: kids)
                  (<ul> NIL (mapc recurse (: kids))) ) ) ) ) ) )

### RUN ###
(de main ()
   (symbols '(family svg pico))
   (pool "family/" *Dbs)
   (unless (val *DB)
      (put>
         (set *DB (request '(+Man) 'nm "Adam"))
         'mate
         (request '(+Woman) 'nm "Eve") )
      (commit) ) )

(de go ()
   (rollback)
   (server 8080 "!treeReport") )
