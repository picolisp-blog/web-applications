### DB ###
(class +Person +Entity)
(rel name (+Need +Sn +Idx +String))  
(rel father (+Joint) kids (+Man))     
(rel mother (+Joint) kids (+Woman))    
(rel partner (+Joint) partner (+Person))
(rel job (+Ref +String))             
(rel birthday (+Ref +Date))           
(rel deathday (+Ref +Date))            
(rel info (+String))                    

(class +Man +Person)
(rel kids (+List +Joint) father (+Person))

(class +Woman +Person)
(rel kids (+List +Joint) mother (+Person)) 

