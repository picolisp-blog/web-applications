# ../pil21/pil family-rest.l -family~main -go +
# http://localhost:8080/?-A67

(allowed NIL
   "@lib.css" "!person.json" )

(load "@lib/http.l" "@lib/xhtml.l" "@lib/json.l")

(symbols 'family 'pico)

### DB ###
(class +Person +Entity)
(rel nm (+Need +Sn +Idx +String))      # Name
(rel pa (+Joint) kids (+Man))          # Father
(rel ma (+Joint) kids (+Woman))        # Mother
(rel mate (+Joint) mate (+Person))     # Partner
(rel job (+Ref +String))               # Occupation
(rel dat (+Ref +Date))                 # born
(rel fin (+Ref +Date))                 # died
(rel txt (+String))                    # Info

(class +Man +Person)
(rel kids (+List +Joint) pa (+Person)) # Children

(class +Woman +Person)
(rel kids (+List +Joint) ma (+Person)) # Children

(dbs
   (0)                                 # @:64
   (2 +Person)                         # A:256
   (3 (+Person nm))                    # B:512
   (3 (+Person job dat fin)) )         # C:512


(de person.json (This)
   (httpHead "application/json" 0)
   (ht:Out *Chunked
      (printJson
         (mapcar
            '((X)
               (cons
                  (cdr X)
                  (let V (car X)
                     (cond
                        ((num? V) (datStr V))  # Can only be date
                        ((isa '+Person V) (; V nm))
                        ((pair V) (mapcar '((This) (: nm)) V) )
                        (T V) ) ) ) )
            (getl This) ) ) ) 
)



### RUN ###
(de main ()
   (symbols '(family pico))
   (pool "family/" *Dbs) )

(de go ()
   (rollback)
   (server 8080 "!person.json") )
