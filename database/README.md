Corresponding Posts:



- ``family-dbmodel.l``: [PicoLisp Explored: Working with multiple database files](https://picolisp-explored/picolisp-explored-working-with-multiple-database-files)
- ``family-treeview-step1.l``: [Creating a User Interface to the Database: Setup](https://picolisp-explored/creating-a-user-interface-to-the-database-setup)
- ``family-treeview.l``, ``family-gui.l``: [Creating A User Interface to the Database: Displaying the Data](https://picolisp-explored/creating-a-user-interface-to-the-database-displaying-the-data)
- ``family-rest.l``: [How to create a RESTful API to the PicoLisp](https://picolisp-explored/how-to-create-a-restful-api-to-the-picolisp-database)
- ``family-edit-step1.l``: [Creating a User Interface for Data Modification, Part 1](https://picolisp-explored/creating-a-user-interface-for-data-modification-part-1)
- ``family-edit.l``: [Creating a User Interface for Data Modification, Part 2](https://picolisp-explored/creating-a-user-interface-for-data-modification-part-2)
- ``family-vulnerable.l``: [How to hack into the PicoLisp Database (and how to prevent it!)](https://picolisp-explored/how-to-hack-into-the-picolisp-database-and-how-to-prevent-it)
- ``family-contemporaries-step1.l``: [Handling Complex Database Queries](https://picolisp-explored.com/handling-complex-database-queries)
- ``family-contemporaries.l``: [Creating PDF Output](https://picolisp-explored.com/creating-pdf-output)
- `` family-original-tutorial.l``: The file from the original PicoLisp tutorial (slightly more complex)
