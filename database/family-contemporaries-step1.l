# 09jul21 Software Lab. Alexander Burger

(allowed ("css/")
   "@lib.css" "!person" "!treeReport" "!contemporaries")

(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l" "@lib/svg.l")

(setq *Css '("@lib.css" "css/bootstrap.css" ))

(symbols 'family 'svg 'pico)

### DB ###
(class +Person +Entity)
(rel nm (+Need +Sn +Idx +String))      # Name
(rel pa (+Joint) kids (+Man))          # Father
(rel ma (+Joint) kids (+Woman))        # Mother
(rel mate (+Joint) mate (+Person))     # Partner
(rel job (+Ref +String))               # Occupation
(rel dat (+Ref +Date))                 # born
(rel fin (+Ref +Date))                 # died
(rel txt (+String))                    # Info

(dm url> (Tab)
   (list "!person" '*ID This) )


(class +Man +Person)
(rel kids (+List +Joint) pa (+Person)) # Children

(class +Woman +Person)
(rel kids (+List +Joint) ma (+Person)) # Children

(dbs
   (0)                                 # @:64
   (2 +Person)                         # A:256
   (3 (+Person nm))                    # B:512
   (3 (+Person job dat fin)) )         # C:512



# Search dialogs
(de choPerson (Dst)
   (choDlg Dst "Persons" '(nm +Person)) )


# Person HTML Page
(de person ()
   (when (app)
      (redirect (baseHRef) *SesId *Url "?*ID=" (ht:Fmt (val *DB))) )
   (action
      (html 0 (; *ID nm) "@lib.css" NIL
         (form NIL
            (<h2> NIL (<id> (: nm)))
            (editButton T)
            (<p> NIL
               (gui '(+E/R +TextField) '(nm : home obj) 40 "Name")
               (gui '(+ClassField) '(: home obj) '(("Male" +Man) ("Female" +Woman))) )
            (<grid> 5
               "Occupation" (gui '(+E/R +TextField) '(job : home obj) 20)
               (choPerson 0)
               "Father" 
               (gui '(+E/R +Obj +TextField) '(pa : home obj) '(nm +Man) 30)
               "born" (gui '(+E/R +DateField) '(dat : home obj) 10)
               (choPerson 0)
               "Mother" 
               (gui '(+E/R +Obj +TextField) '(ma : home obj) '(nm +Woman) 30)
               "died" (gui '(+E/R +DateField) '(fin : home obj) 10)
               (choPerson 0)
               "Partner" 
               (gui '(+E/R +Obj +TextField) '(mate : home obj) '(nm +Person) 30) )
            (--)
            (gui '(+E/R +Chart) '(kids : home obj) 7
               '((This) (list NIL This (: dat) (: pa) (: ma)))
               cadr )
            (<table> NIL NIL
               '(NIL (NIL "Children") (NIL "born") (NIL "Father") (NIL "Mother"))
               (do 6
                  (<row> NIL
                     (choPerson 1)
                     (gui 2 '(+Obj +TextField) '(nm +Person) 20)
                     (gui 3 '(+Lock +DateField) 10)
                     (gui 4 '(+ObjView +TextField) '(: nm) 20)
                     (gui 5 '(+ObjView +TextField) '(: nm) 20)
                     (gui 6 '(+DelRowButton))
                     (gui 7 '(+BubbleButton)) ) )
               (<row> NIL NIL (scroll 6 T)) )
            (----)
            (gui '(+E/R +TextField) '(txt : home obj) 40 4)
            (----) 
           (gui '(+Rid +Button) "Contemporaries"
               '(url "!contemporaries" (: home obj)) )
            (gui '(+Rid +Button) "Tree View"
               '(url "!treeReport" (: home obj)) ) ) ) ) )



# Tree display of a person's descendants
(de treeReport (This)
   (html 0 "Family Tree View" *Css NIL

      (<div> "container-sm"
         (<h1> "d-flex m-5 justify-content-center" "Family Tree View")

         (<div> "container-sm bg-light p-3"
            (<h4> "my-5 text-bold"
               (ht:Prin
                  "Family tree of "
                  (: nm)
                  " ("
                  (or (datStr (: dat)) "<unknown date>")
                  " to "
                  (or (datStr (: fin)) "<unknown date>" )
                  ") " ) )

            (<ul> "list-group list-group-flush"
               (recur (This)
                  (when (try 'url> This 1)
                     (<li> "list-group-item list-group-item-action"
                        (<href> (: nm) (mkUrl @))
                        (when (try 'url> (: mate) 1)
                           (prin " -- ")
                           (<href> (: mate nm) (mkUrl @)) ) ) )

                  (when (: kids)
                     (<ul> NIL (mapc recurse (: kids))) ) ) ) ) ) ) )



(de contemporaries (*ID)
   (action
      (html 0 "Contemporaries" "@lib.css" NIL
         (form NIL
            (<h3> NIL (<id> "Contemporaries of " (: nm)))

            (ifn (: obj dat)
               (<h3> NIL (ht:Prin "No birth date for " (: obj nm)))

               (gui '(+QueryChart) 12
                  '(goal
                     (quote
                        @Obj (: home obj)
                        @Dat (: home obj dat)
                        @Beg (- (: home obj dat) 36525)
                        @Fin (or (: home obj fin) (+ (: home obj dat) 36525))
                        (db dat +Person (@Beg . @Fin) @@)
                        (different @@ @Obj)
                        (^ @ (>= (; @@ fin) @Dat))
                        (^ @ (<= (; @@ dat) @Fin)) ) )
                  7
                  '((This)
                     (list This (: job) (: dat) (: fin) (: pa) (: ma) (: mate)) ) )
               (<table> NIL (pack (datStr (: obj dat)) " - " (datStr (: obj fin)))
                  (quote
                     (NIL "Name") (NIL "Occupation") (NIL "born") (NIL "died")
                     (NIL "Father") (NIL "Mother") (NIL "Partner") )
                  (do 12
                     (<row> NIL
                        (gui 1 '(+ObjView +TextField) '(: nm))
                        (gui 2 '(+TextField))
                        (gui 3 '(+DateField))
                        (gui 4 '(+DateField))
                        (gui 5 '(+ObjView +TextField) '(: nm))
                        (gui 6 '(+ObjView +TextField) '(: nm))
                        (gui 7 '(+ObjView +TextField) '(: nm)) ) ) )
               (scroll 12) ) ) ) ) )



### RUN ###
(de main ()
   (symbols '(family svg pico))
   (pool "family/" *Dbs)
   (unless (val *DB)
      (put>
         (set *DB (request '(+Man) 'nm "Adam"))
         'mate
         (request '(+Woman) 'nm "Eve") )
      (commit) ) )

(de go ()
   (rollback)
   (server 8080 "!person") )
