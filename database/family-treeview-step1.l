# 15oct21 Software Lab. Alexander Burger

# ../pil21/pil family-treeview-step1.l -family~main -go +
# http://localhost:8080/?-A67


(allowed ("css/")
   "@lib.css" "!treeReport" )

(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l" )

(setq *Css '("@lib.css" "css/bootstrap.css"))

(symbols 'family 'pico)

### DB ###
(class +Person +Entity)
(rel nm (+Need +Sn +Idx +String))      # Name
(rel pa (+Joint) kids (+Man))          # Father
(rel ma (+Joint) kids (+Woman))        # Mother
(rel mate (+Joint) mate (+Person))     # Partner
(rel job (+Ref +String))               # Occupation
(rel dat (+Ref +Date))                 # born
(rel fin (+Ref +Date))                 # died
(rel txt (+String))                    # Info


(class +Man +Person)
(rel kids (+List +Joint) pa (+Person)) # Children

(class +Woman +Person)
(rel kids (+List +Joint) ma (+Person)) # Children

(dbs
   (0)                                 # @:64
   (2 +Person)                         # A:256
   (3 (+Person nm))                    # B:512
   (3 (+Person job dat fin)) )         # C:512


### TREE REPORT - PROOF OF CONCEPT ###
(de treeReport ( This )
   (html 0 "Family Tree View" *Css NIL
      (<h1> NIL (prinl (: nm))) ) )  

### RUN ###
(de main ()
   (symbols '(family pico))
   (pool "family/" *Dbs)
   (unless (val *DB)
      (put>
         (set *DB (request '(+Man) 'nm "Adam"))
         'mate
         (request '(+Woman) 'nm "Eve") )
      (commit) ) )

(de go ()
   (rollback)
   (server 8080 "!treeReport") )
