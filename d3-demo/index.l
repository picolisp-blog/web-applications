# ~/pil21/pil index.l -go   

(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l")

(setq *Css '("@lib.css" "css/bootstrap.css" "css/custom.css"))

(setq *Data (90 230 180 300 110 195) *Width 400 *Height 400 )


(de work ()
   (and (app) *Port% (redirect (baseHRef) *SesId *Url))
   (action
      (html 0 "D3.JS in PicoLisp" *Css NIL
         # Java Script Sources
         (javascript "https://d3js.org/d3.v7.min.js")
         (javascript "js/barGraph.js")
         (javascript "js/xyGraph.js")
         (javascript "js/pieGraph.js")
         # Title
         (<div> "bg-light container-sm border mt-2 pt-2"
            (<div> "d-flex flex-column "
               (<h1> "d-flex my-4 justify-content-center" "Data Visualization with D3 and PicoLisp") )
            # display and modify data
            (form NIL
               (<div> "row pt-5 my-2"
                  (<div> "col-auto"
                     (<p> "ml-3 font-weight-bold" "Current input data: " (print *Data)) ) )
               (<div> "row my-2"
                  (<div> "col-auto py-3 mx-1"
                     (<p> "ml-3" "Add new value: ") )
                  (<div> "col-auto p-3"
                     (gui '(+Init +NumField) 100 10 ) )
                   (<div> "col-auto p-3 "
                     (gui '(+Style +Button) "button-icon" T "img/add.png" 
                        '(post (push '*Data (val> (field -1))) ) ) ) ) ) 
            # graphs
            (<tab>
               ("XY Plot"
                  (<div> "m-5 d-flex flex-column bg-white"
                     (<div> '((id . "xyGraph"))) ) 
                  (<javascript> 
                     " var data = [" (glue ", " *Data) "]; "
                     "drawXYGraph(data, xyGraph, " *Width "," *Height "); " ) ) 
               ("Bar Chart"
                  (<div> "m-5 d-flex flex-column bg-white"
                     (<div> '((id . "barGraph"))) ) 
                  (<javascript> 
                     " var data = [" (glue ", " *Data) "]; "
                     "drawBarChart(data, barGraph, " *Width "," *Height "); " ) ) 
               ("Pie Chart"
                  (<div> "m-5 d-flex flex-column bg-white"
                     (<div> '(id . "pieGraph")) ) 
                  (<javascript> 
                     " var data = [" (glue ", " *Data) "]; "
                     "drawPieGraph(data, pieGraph, " (* 2 *Width) ", " *Height ");" ) ) ) ) ) ) )


(de go ()
   (server 8080 "!work") )
