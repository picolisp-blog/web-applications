function drawXYGraph(data, id, w, h) {

   var length = data.length;

   var svg = d3.select(id)
      .append("svg")
      .attr("width", w)
      .attr("height", h);

   var margin = svg.attr("width") * 0.2,
      width = svg.attr("width") - margin,
      height = svg.attr("height") - margin;

   var xScale = d3.scaleLinear().range([0, width]),
       yScale = d3.scaleLinear().range([height, 0]);

   var g = svg.append("g")
         .attr("transform", `translate(${margin/2}, ${margin/2})`)

   xScale.domain([0, length-1]);
   yScale.domain([0, d3.max(data)]);

   g.append("g")
         .attr("transform", `translate(0, ${height})`)
         .call(d3.axisBottom(xScale)
            .tickValues(xScale.ticks().filter(tick => Number.isInteger(tick)))
            .tickFormat(d3.format("d")));

   g.append("g")
         .call(d3.axisLeft(yScale));

   var valueline = d3.line()
      .x( (d, i) => xScale(i))
      .y( d => yScale(d))

   g.append("path")
         .attr("class", "line")
         .attr("d", valueline(data));

   g.selectAll(".circle")
         .data(data)
         .enter()
         .append("circle")
         .attr("class", "circle")
         .attr("r", 2)
         .attr("cx", (d, i) => xScale(i))
         .attr("cy", (d) => yScale(d))
         .append("title")
         .text( (d) => d);

}
