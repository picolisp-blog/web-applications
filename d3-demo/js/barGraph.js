// https://www.tutorialsteacher.com/d3js/create-bar-chart-using-d3js


function drawBarChart(data, id, w, h) {
   
   var svg = d3.select(id)
      .append("svg")
      .attr("width", w)
      .attr("height", h);

   var margin = svg.attr("width") * 0.2,
      width = svg.attr("width") - margin,
      height = svg.attr("height") - margin;

   // axis
   var xScale = d3.scaleBand().range ([0, width]).padding(0.4),
       yScale = d3.scaleLinear().range([height, 0]);

   var g = svg.append("g")
         .attr("transform", `translate(${margin/2}, ${margin/2})`)

   xScale.domain(data.map((d,i) => i ));
   yScale.domain([0, d3.max(data)]);

   g.append("g")
         .attr("transform", `translate(0, ${height})`)
         .call(d3.axisBottom(xScale));

   g.append("g")
         .call(d3.axisLeft(yScale));

   g.selectAll(".bar")
         .data(data)
         .enter()
         .append("rect")
         .attr("class", "bar")
         .attr("x", (d, i) =>  xScale(i))
         .attr("y", (d) => yScale(d))
         .attr("width", xScale.bandwidth())
         .attr("height", (d) => (height - yScale(d)))
         .append("title")
         .text( (d) => d);

}
