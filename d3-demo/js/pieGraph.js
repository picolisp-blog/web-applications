// https://www.tutorialsteacher.com/d3js/create-pie-chart-using-d3js
// https://bl.ocks.org/farazshuja/afd5e5f492fd00dd0ac4644d53716f4e

function drawPieGraph(data, id, w, h) {
   console.log(w);

   var sum = data.reduce((pv, cv) => pv + cv, 0),
      margin = w * 0.2,
      radius = Math.min(w, h) / 2;

   var svg = d3.select(id)
      .append("svg")
      .attr("width", w)
      .attr("height", h)
      .append("g")
      .attr("transform", `translate(${w/2/2}, ${h/2})`);

   var color = d3.scaleOrdinal(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", 
                                 "#a05d56", "#d0743c", "#ff8c00"]);

   var pie = d3.pie();

   var arc = d3.arc()
         .innerRadius(radius*0.45)
         .outerRadius(radius*0.65);

   var g = svg.selectAll(".arc")
         .data(pie(data))
         .enter()
         .append("g")
         .attr("class", "arc");

   g.append("path")
         .attr("fill", (d, i) => color(i))
         .attr("d", arc)
         .append("title")
         .text((d, i) => "datapoint " + i + ", value: "  + d.data)

   g.append("text")
         .attr("transform", (d) => {
             var _d = arc.centroid(d);
                    _d[0] *= 1.5;   
                    _d[1] *= 1.5;  
                    return "translate(" + _d + ")"; 
         })
         .attr("dy", ".50em")
         .style("text-anchor", "middle")
         .text((d) => Math.round((d.data / sum) * 100) + "%");

   g.append("text")
         .attr("transform", (d) => {
             var _d = arc.centroid(d);
                    _d[0] *= 1;   
                    _d[1] *= 1;  
                    return "translate(" + _d + ")"; 
         })
         .attr("dy", ".50em")
         .style("text-anchor", "middle")
         .text((d) => d.data );


   g.append("text")
         .attr("text-anchor", "middle")
         .attr('font-size', '2em')
         .attr('y', 10)
         .text(sum);


   var legendG = svg.selectAll(".legend") 
     .data(pie(data))
     .enter()
     .append("g")
     .attr("transform", (d, i) => `translate(${0.3*w},${i*20 - 90})`)
     .attr("class", "legend");   

   legendG.append("rect") 
     .attr("width", 10)
     .attr("height", 10)
     .attr("fill", (d, i) => color(i))

   legendG.append("text") 
     .text((d, i) => "Datapoint " + i )
     .style("font-size", 12)
     .attr("y", 10)
     .attr("x", 11);


}
