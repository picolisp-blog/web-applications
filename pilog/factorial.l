(be factorial (0 1) T)

(be factorial (@N @X)
   (^ @A (dec @N))
   (factorial @A @B)
   (^ @X (* @N @B)) )

(println 
   (car
      (solve '((factorial 5 @X)) @X) ) )

(bye)
