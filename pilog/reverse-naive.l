(be reverse (NIL NIL))

(be reverse ((@A . @X) @R) 
   (reverse @X @Z)
   (append @Z (@A) @R))
