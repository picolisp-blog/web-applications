Pilog stands for PicoLisp Prolog. The corresponding posts:


- ``friends.l``: [Learning Pilog - 2: Facts, Rules, Queries](https://picolisp-explored/learning-pilog-2-facts-rules-queries)
- ``crossword.l``, ``magic.l``: [Learning Pilog - 3: Unification and Proof Search](https://picolisp-explored/learning-pilog-3-unification-and-proof-search)
- ``digestion-wrong.l``, ``digestion.l``, ``travel.l``: [Learning Pilog - 4: Recursion](https://picolisp-explored/learning-pilog-4-recursion)
- ``translator.l``: [Learning Pilog - 5: Lists](https://picolisp-explored/learning-pilog-5-lists)
- ``palindrome.l``, ``reverse.l``, ``reverse-naive.l``: [Learning Pilog - 6: More Lists](https://picolisp-explored/learning-pilog-6-more-lists)
- ``maxi.l``, ``neg.l``: [Learning Pilog - 7: Cuts and Negations](https://picolisp-explored/learning-pilog-7-cuts-and-negations)
- ``factorial.l``: [How to use Pilog in PicoLisp](https://picolisp-explored/how-to-use-pilog-in-picolisp)

