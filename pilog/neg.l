# Facts

(be insect (Spider))
(be pet (Cat))
(be pet (Dog))
(be cattle (Cow))


# Rules

(be animal (@X)
   (insect @X) )

(be animal (@X)
   (pet @X) )

(be animal (@X)
   (cattle @X) )


# Mary likes all animals except insects.
(be likes (Mary @X)
   (insect @X)
   T
   (fail) ) 

(be likes (Mary @X)
   (animal @X) )
