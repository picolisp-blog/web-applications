(be accRev ((@H . @T) @A @R)
   (accRev @T (@H . @A) @R) )

(be accRev (NIL @A @A))

(be reverse (@L @R) 
   (accRev @L NIL @R))
