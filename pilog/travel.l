(be directTrain (Saarbruecken Dudweiler))
(be directTrain (Forbach Saarbruecken))
(be directTrain (Freyming Sorbach))
(be directTrain (StAvold Freyming))
(be directTrain (Fahlquemont StAvold))
(be directTrain (Metz Fahlquemont))
(be directTrain (Nancy Metz))

(be travelBetween (@X @Y)
   (directTrain @X @Y))

(be travelBetween (@X @Y)
   (directTrain @X @Z)
   (travelBetween @Z @Y))

