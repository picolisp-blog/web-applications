Corresponding posts:


- ``simple-windowed-application.l``: [Web Apps: A Simple Windowed Application](https://picolisp-explored/webapps-a-simple-windowed-application)
- ``GUI-Component-Interaction.l``: [Web Apps: GUI Component Interaction](https://picolisp-explored/web-apps-gui-component-interaction)
- ``GUI-enabling-disabling.l``: [Web Apps: Enabling and Disabling of Controls](https://picolisp-explored/web-apps-gui-enablingdisabling-of-controls)
- ``animation.l``: [Web Apps: An Animation](https://picolisp-explored/web-apps-an-animation)
- ``forest-fire.l``: A Canvas example, [The Forest Fire Model: Let it burn!](https://picolisp-explored.com/the-forest-fire-model-let-it-burn)

