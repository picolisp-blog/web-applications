(setq *Css '("@lib.css" "css/bootstrap.css" "css/custom.css"))

(ifn *Str
   (setq *Str (chop "Hello World! ")) )

(ifn *Dir
  (one *Dir) )

(and (app) *Port% (redirect (baseHRef) *SesId *Url))


(action 
   (html 0 "Animation" *Css "bg-dark p-5 text-center text-white neon-text"
      (form NIL  
         (gui '(+Style +View +TextField) "mt-5 blink large-text" '(pack (do *Dir (rot *Str)))) 
         (<div> "row"
            (<div> "col justify-content-center text-center" 
               (gui '(+Style +Tip +Button) "icon-style text-center btn-neon" "change rotation" T 
                  '(if (= 1 *Dir) "img/rotate-left.png" "img/rotate-right.png")
                  '(setq *Dir (if (= 1 *Dir) 12 1)) )  ) )
            (gui '( +Style +Click +Auto +Button) "invisible" 500 'This 500 "Start animation" ) ) 

      (<div> "d-flex flex-column mx-5 mt-5"
         (<p> NIL "A Task from the "
            (<href> "Rosetta Code Project" "http://rosettacode.org/wiki/Animation#PicoLisp" "_blank" ) ) )
      (<div> "d-flex flex-column mx-5 my-2"
         (<p> NIL "View source code on "
            (<href> "Gitlab"
               "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/rosetta-code/animation.l" "_blank") ) ) ) )



