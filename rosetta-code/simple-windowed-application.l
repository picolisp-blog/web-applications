(setq *Css '("@lib.css" "css/bootstrap.css"))
(default *Count 0)

(and (app) *Port% (redirect (baseHRef) *SesId *Url))

(action
   (html 0 "Simple Windowed Application" *Css NIL 

      (<div> "container-sm"
         (<h1> "d-flex m-5 justify-content-center" "A Simple Windowed Application") 

         (<div> "d-flex flex-column bg-light mx-5"
            (<div> "d-flex justify-content-center text-center my-5"
            (form NIL
               (<div> "row"
                  (<div> "col"
                     (gui '(+Init +Style +TextField) "There have been no clicks yet" "my-5")(<br>) ) )
               (<div> "row"
                  (<div> "col"
                     (gui '(+JS +Style +Button) "my-5 p-3" "Click me!" 
                        '(set> (field -1) (pack "Clicked " (inc '*Count) " times")) ) ) ) ) ) ) 

         (<div> "d-flex flex-column mx-5 my-2"
            (<p> NIL "A Task from the "
               (<href> "Rosetta Code Project" "http://rosettacode.org/wiki/Simple_windowed_application#PicoLisp" 
                  "_blank" ) ) )
         (<div> "d-flex flex-column mx-5 my-2"
            (<p> NIL "View source code on "
               (<href> "Gitlab" 
                  "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/rosetta-code/simple-wndowed-application.l" 
                  "_blank") ) ) ) ) )

