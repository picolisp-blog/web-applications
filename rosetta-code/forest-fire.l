(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l" "@lib/canvas.l" "@lib/simul.l")
(import simul~grid simul~west simul~east simul~south simul~north)

(setq *CanvasDX 480  *CanvasDY 360)

(de drawCanvas (Id Dly)
   (for Col *FireGrid
      (for This Col
         (=: next
            (cond
               ((: burn) NIL)
               ((: tree)
                  (if
                     (or
                        (find  # Neighbor burning?
                           '((Dir) (get (Dir This) 'burn))
                           (quote
                              west east south north
                              ((X) (south (west X)))
                              ((X) (north (west X)))
                              ((X) (south (east X)))
                              ((X) (north (east X))) ) )
                        (=0 (rand 0 9999)) )
                     'burn
                     'tree ) )
               (T (and (=0 (rand 0 99)) 'tree)) ) ) ) )
   (for Col *FireGrid 
      (for This Col
         (if (: next)
            (put This @ T)
            (=: burn)
            (=: tree) ) ) )
   (make
      (csClearRect 0 0 *CanvasDX *CanvasDY)
      (csFillStyle "green")
      (csDrawDots 1 1
         (make
            (for (X . Col) *FireGrid
               (for (Y . This) Col
                  (and (: tree) (link X Y)) ) ) ) )
      (csFillStyle "red")
      (csDrawDots 3 3
         (make
            (for (X . Col) *FireGrid
               (for (Y . This) Col
                  (and (: burn) (link X Y)) ) ) ) ) ) )


(de forest ()
   (and (app) *Port% (redirect (baseHRef) *SesId *Url))
   (action
      (html 0 "Forest Fire" "@lib.css" NIL
         (form NIL
            (<h3> NIL "Forest Fire")
               (for Col (setq *FireGrid (grid *CanvasDX *CanvasDY))
                  (for This Col
                     (=: tree (rand T)) ) ) 
            (--)
            (<canvas> "$*FireGrid" *CanvasDX *CanvasDY)
            (javascript NIL "onload=drawCanvas('$*FireGrid', 40)") ) )) )

(de go()
   (server 8080 "!forest"))

