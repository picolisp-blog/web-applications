#!/usr/bin/picolisp /usr/lib/picolisp/lib.l
(load "@ext.l" "@lib/http.l" "@lib/xhtml.l" "@lib/form.l")
 
(setq *Css '("@lib.css" "css/bootstrap.css"))
(zero *Count)

(de start ()
   (app)
   (action
      (html 0 "GUI component interaction" *Css NIL 
         (<div> "d-flex flex-column"
            (<h1> "d-flex m-5 justify-content-center" "GUI Component Interaction") )
         (<div> "d-flex flex-column bg-light text-center mx-5"
            (form NIL
               (<div> "d-flex justify-content-center text-center my-5"
                  (gui '(+Var +Style +NumField) '*Count "mx-2" 20 "Value: ") 
                  (gui '(+JS +Style +Button) "mx-1" "increment" '(inc '*Count)) 
                  (gui '(+JS +Style +Button) "mx-1" "random" 
                     '(ask "Reset to a random value?" (setq *Count (rand))) ) ) ) ) 

         (<div> "d-flex flex-column mx-5 my-2"
            (<p> NIL "A Task from the "
               (<href> "Rosetta Code Project" "http://rosettacode.org/wiki/GUI_component_interaction#PicoLisp" 
                  "_blank" ) ) )
         (<div> "d-flex flex-column mx-5 my-2"
            (<p> NIL "View source code on "
               (<href> "Gitlab" 
                  "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/gui-elements/simple-post.l" 
                  "_blank") ) ) ) ) )

(server 8080 "!start")
(wait)
