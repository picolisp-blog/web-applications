#!/usr/bin/picolisp /usr/lib/picolisp/lib.l
(load "@ext.l" "@lib/http.l" "@lib/xhtml.l" "@lib/form.l")
 
(setq *Css '("@lib.css" "css/bootstrap.css" "css/icons.css"))
(zero *Count)

(de start ()
   (app)
   (action
      (html 0 "GUI Enabling/Disabling" *Css NIL
         (<div> "container-sm"
            (<h1> "d-flex m-5 justify-content-center" "GUI Enabling/Disabling of Controls") 

            (<div> "d-flex flex-column text-center mx-5 "
               (form NIL
                  (<div> "d-flex justify-content-center text-center my-5"
                     (gui '(+Able +JS +Style +Button) '(gt0 *Count) "icon-style" T "img/minus.png" '(dec '*Count)) 
                     (gui '(+Var +Style +NumField) '*Count "text-center" 2)
                     (gui '(+Able +JS +Style +Button) '(> 10 *Count) "icon-style" T "img/add.png" '(inc '*Count)) ) ) )


            (<div> "d-flex flex-column mx-5 my-2"
               (<p> NIL "A Task from the "
                  (<href> "Rosetta Code Project" "http://rosettacode.org/wiki/GUI_enabling/disabling_of_controls#PicoLisp" "_blank" ) ) 
               (<p> NIL "View source code on "
                  (<href> "Gitlab" 
                     "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/rosetta-code/GUI-enabling-disabling.l"
                     "_blank") ) ) ) ) ) )


(server 8080 "!start")
(wait)
