(html 0 "Hello" NIL 'foo
   (<h1> NIL "Hello World!") 
   (<h2> NIL "This is a first test page.") 
   (<p> NIL 
      "This is a text block. "
      (<strong> "Still the same text block. ") 
      "Still a text block.")
   (<hr>)
   (<div> NIL
      (<h5> NIL "Let's test how nested divs look like.")
      (<div> NIL "This is a div in a div."
      (<ul> NIL
         (<li> NIL "Monday")
         (<li> NIL "Tuesday"
         (<ul> NIL
            (<li> NIL "Morning")
            (<li> NIL "Evening") ) ) ) ) ) )
