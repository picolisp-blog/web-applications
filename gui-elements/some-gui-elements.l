(setq *Css '("@lib.css" "css/bootstrap.css"))

(app)

(action
   (html 0 "Some GUI examples" *Css NIL 

      (<div> "container-sm "
         (<h1> "d-flex m-5 justify-content-center" "The +TextField and +Button class") 

         (<div> "d-flex bg-light justify-content-center pt-5"

            (form NIL
               (<grid> 1 
                  (<h5> "my-2" "Plain Textfield")
                  (gui '(+Style +TextField) "mb-4") 

                  (<h5> "my-2" "Textfield, length 10")
                  (gui '(+Style +TextField) "mb-4" 10) 

                  (<h5> "my-2" "Textfield, length 10, 4 lines")
                  (gui '(+Style +TextField) "mb-4" 10 4) 

                  (<h5> "my-2" "Textfield, dropdown")
                  (gui '(+Style +TextField) "mb-4" '("Value1" "Value2" "Value3")) 

                  (<h5> "my-2" "Textfield, labels")
                  (gui '(+Style +TextField) "mb-5" '("Value1" "Value2" "Value3") "Please choose: ") 

                  (<h5> "my-2" "Print content to standard error, referenced by ALIAS:")
                  (gui 'a '(+Style +TextField) "mb-2" 30 "Enter your text: ") 
                  (gui '(+Style +Button) "mb-5" "Print" '(msg (val> (: home a))) ) 

                  (<h5> "my-2" "Print content to standard error, referenced by relative position:")
                  (gui '(+Style +TextField) "mb-2" 30 "Enter your text: ") 
                  (gui '(+Style +Button) "mb-5" "Print" '(msg (val> (field -1))))  ) ) ) 


         (<p> "pt-3" "View source code on " 
            (<href> "Gitlab" "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/gui-elements/some-gui-elements-post.l" "_blank") ) ) ) )
