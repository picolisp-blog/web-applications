(setq *Css '("@lib.css" "css/bootstrap.css"))

(app)

(html 0 "Simple Post" *Css NIL 
   (<div> "d-flex flex-column"
      (<h1> "d-flex m-5 justify-content-center" "A little POST example with global variables") )
   (<div> "d-flex flex-column bg-light mx-5"
      (<div> "d-flex justify-content-center my-5"
         (<post> NIL *Url
            (<label> "mx-2" "First Name: ")
            (<field> 30 '*FirstName)
            (<br>)
            (<label> "mx-2" "Last Name: ")
            (<field> 30 '*LastName)
            (<submit> "Send" ) ) ) ) 
   (<div> "d-flex flex-column bg-light mx-5"
      (<div> "d-flex justify-content-center my-5"
         (<h3> "d-flex m-1 justify-content-center" 
            (if *FirstName 
               (prinl "Hello, " *FirstName " " *LastName "!") ) ) ) )


   (<div> "d-flex flex-column m-5"
      (<p> NIL "View source code on " 
         (<href> "Gitlab" "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/gui-elements/stateful-post.l" "_blank") ) ) ) 
