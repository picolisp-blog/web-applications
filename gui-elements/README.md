These examples belong to the introduction of the PicoLisp GUI framework. Corresponding posts:


- ``simple-post.l``: [Creating HTML Forms, Part 1](https://picolisp-explored/web-application-programming-in-picolisp-creating-html-forms-part-1)
- ``stateful-post.l``, ``stateful-post-test.l``, ``stateful-post-gui.l``: [Introducing the GUI Framework](https://picolisp-explored/web-application-programming-in-picolisp-introducing-the-gui-framework)
- ``prefix-classes.l``, ``some-gui-elements.l``: [Prefix Classes](https://picolisp-explored/web-application-programming-in-picolisp-prefix-classes)
- ``more-gui-elements.l``: [Some more GUI elements](https://picolisp-explored/web-application-programming-in-picolisp-some-more-gui-elements)

