(setq *Css '("@lib.css" "css/bootstrap.css"))

(ifn *FirstName
   (setq *FirstName "Your initial text") )

(and (app) *Port% (redirect (baseHRef) *SesId *Url))

(action
   (html 0 "Prefix classes" *Css NIL 
      (<div> "d-flex flex-column"
         (<h1> "d-flex m-5 justify-content-center" "Demonstration of Prefix Classes") )

      (<div> "d-flex flex-column bg-light mx-5"
         (<div> "d-flex justify-content-center my-5"

            (form NIL
               (<grid> 1 
                  (<h5> "my-3" "+Init and +Cue Classes")
                  (gui '(+Init +Style +TextField) "This is the initial text" "mx-5 mb-2" 30 "Enter your text: ") 
                  (gui '(+Cue +Style +TextField) "Please enter some text here" "mb-5" 30 "Enter your text: ")    

                  (<h5> "my-3" "+Lock Class")
                  (gui '(+Lock +Style +TextField) "mx-5 mb-2" 30 "Locked Textfield")     
                  (gui '(+Lock +Style +Button) "mb-5" "Locked Button" ) 

                  (<h5> "my-3" "+Able Class")
                  (gui '(+Able +Style +TextField) '(< 3 5) "mx-5 mb-2" 30 "Enabled Textfield")     
                  (gui '(+Able +Style +Button) '(< 5 3) "mb-5" "Disabled Button" ) 

                  (<h5> "my-3" "+JS Class")
                  (gui '(+Style +TextField) "mb-2" 30 "Enter your text: ")   
                  (gui '(+Style +Button) "mb-2" "Print to StdErr" '(msg (val> (field -1)))) 
                  (gui '(+Style +JS +Button) "mb-5" "+JS Print to StdErr" '(msg (val> (field -2))))

                  (<h5> "my-3" "+Var Class")
                  (gui '(+Style +Var +TextField) "mb-2" '*FirstName 30 "Enter your text: ")   
                  (gui '(+Style +View +TextField) "mb-2" '(pack "This will change: " *FirstName ))   
                  (<p> "" (prinl "This text will not change: " *FirstName)) 
                  (gui '(+Style +JS +Button) "mb-3" "+JS Print" '(val> (field -3))) ) ) ) )

      (<div> "d-flex flex-column bg-light mx-5"
         (<div> "d-flex justify-content-center my-5"
            (form NIL
               (<grid> 1 
                  (<h5> "my-3" "+Val Class")
                  (gui '(+Style +Val +NumField) "mx-5 mb-2" '((N) (* N N)) 30 "Enter a number: ")
                  (gui '(+Style +Button) "mb-3" "get square value" ) ) ) ) )

      (<div> "d-flex flex-column bg-light mx-5"
         (<div> "d-flex justify-content-center my-5"
            (form NIL
               (<grid> 1 
                  (<h5> "my-3" "+Set Class")
                  (gui '(+Style +Set +NumField) "mx-5 mb-2" '((N) (* N N)) 30 "Enter a number: ")
                  (gui '(+Style +Button) "mb-3" "get square value" )) ) ) )   

      (<div> "d-flex flex-column bg-light mx-5"
         (<div> "d-flex justify-content-center my-5"
            (form NIL
               (disable (< 3 5))
                  (<grid> 1 
                     (<h5> "my-3" "+Rid Class in disabled form")
                     (gui '(+Init +Style +TextField) "Disabled textfield" "mx-5 mb-2" 30 "Enter your text: ") 
                     (gui '(+Rid +Init +Style +TextField) "Enabled by +Rid" "mx-5 mb-2" 30 "Enter your text: ") ) ) ) )

   (<hr>)

   (<div> "d-flex flex-column m-5"
      (<p> NIL "View source code on " 
         (<href> "Gitlab" "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/gui-elements/prefix-classes.l" "_blank") ) ) ) )  
