(setq *Css '("@lib.css" "css/bootstrap.css"))

(app)

(action
   (html 0 "Simple Session" *Css NIL 
      (<div> "container-sm"
         (<h1> "d-flex m-5 justify-content-center" "A little POST example with the GUI framework") 

         (<div> "d-flex bg-light justify-content-center my-5 py-5"
            (form NIL
               (gui 'a '(+Style +TextField) "mx-5" 30 "First Name")                 
               (<br>)
               (gui 'b '(+Style +TextField) "mx-5 my-1" 30 "Last Name")                 
               (gui '(+Button) "Send" ) 
               ) )  

      (<p> NIL "View source code on " 
         (<href> "Gitlab" "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/gui-elements/simple-post.l" "_blank") ) ) ) ) 
