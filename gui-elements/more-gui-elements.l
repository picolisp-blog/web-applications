(setq *Css '("@lib.css" "css/bootstrap.css"))

(app)

(action
   (html 0 "Simple Session" *Css NIL 
      (<div> "container-sm"

         (<h1> "d-flex m-5 justify-content-center" "A little POST example with the GUI framework") 

         (<div> "d-flex justify-content-center my-5 bg-light"
            (form NIL

               (<h5> "my-3" "<img> function without link")
               (<img> "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/PicoLisp_Logo.svg/1200px-PicoLisp_Logo.svg.png" "PicoLisp" NIL 100 100)

               (<h5> "mt-5 mb-3" "<img> function with link")
               (<img> "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/PicoLisp_Logo.svg/1200px-PicoLisp_Logo.svg.png" "PicoLisp" "http://picolisp.com" 100 100)

               (<h5> "mt-5 mb-3" "+Img class")
               (gui '(+Style +Init +Img) "mb-3" "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/PicoLisp_Logo.svg/1200px-PicoLisp_Logo.svg.png" "PicoLisp" 100 100 )

               (<h5> "mt-5 mb-3" "+Radio class")
                  (<label> "mr-2" "Radio A") (gui '(+Var +Radio) '*DemoRadio NIL "A")
                  (<br>)
                  (<label> "mr-2" "Radio B") (gui '(+Radio)  -1 "B")
                  (<br>)
                  (<label> "mr-2" "Radio C") (gui '(+Radio)  -2 "C")

               (<h5> "mt-5 mb-3" "+RgbPicker class")
                  (<label> "mr-2" "Choose a color: ") (gui '(+Var +RgbPicker) '*Color)
                  (gui '(+Button) "Submit")
                  (<p> "mb-5" (prinl "You choose: " *Color))

               (<h5> "mt-5 mb-3" "+Checkbox class")
                  (gui '(+Checkbox) "I don't like Mondays")

               (<h5> "mt-5 mb-3" "+Button subclass")
                  (gui '(+BubbleButton)) (<label> "mr-2" "Shift-Row-Up Button") (<br>)
                  (gui '(+DelRowButton)) (<label> "mr-2" "Delete-Row Button") (<br>)
                  (gui '(+InsRowButton)) (<label> "mr-2" "Insert-Row Button") (<br>)
                  (gui '(+UndoButton)) (<label> "mr-2" "Undo Button") (<br>)
                  (gui '(+ShowButton)) (<label> "mr-2" "Show Button") (<br>)
            
               (<h5> "mt-5 mb-3" "+TextField subclass")
                  (gui '(+Style +UpField) "mx-3 mb-2" 20 "Upload Field: ") (<br>)
                  (gui '(+Style +PwField) "mx-3 mb-2" 20 "Password Field: ") (<br>) ) )

         (<p> NIL "View source code on " 
            (<href> "Gitlab" "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/gui-elements/more-gui-elements.l" "_blank") ) ) ) )
