# Web Applications

These are examples from my blog "www.picolisp-explored.com" regarding web application and database programming. These examples usually require a folder structure because the CSS-file and images are stored separately.

Further examples can be found in the ["Single Page Scripts" Repository](https://gitlab.com/picolisp-blog/single-plage-scripts). The link to the corresponding posts can be found inside the folders.

- ``ERP-example/``: An example of a minimal full-stack "Enterprise-Resource-Planning" app example.
- ``addressbook/``: An example of a minimal full-stack addressbook app example.
- ``basic/``: Some simple helloworld-file.
- ``bootstrap-example/``: How to include Bootstrap CSS (or other CSS resources)
- ``canvas/`` : how to use HTML Canvas in PicoLisp
- ``database/``: Corresponding scripts to the database tutorial
- ``docs/``: Tutorials and documentation for pil21
- ``gui-elements/``: Some basic GUI-elements from the PicoLisp GUI Framework
- ``pilog/``: Examples for PicoLisp Pilog (more can be found in the Rosetta Code folder of the "single-page-sccripts" repository)
- ``rosetta-code/``: Examples from the Rosetta Code which require more than one file.
- ``todo-list-example/``: An example of a minimal Todo App.
- ``d3-demo/``: A demo how to visualize data with PicoLisp and D3js.
