(allowed ("css/" "img/")
   "!todoList" "@lib.css" )

(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l")

(setq *Css '("@lib.css" "css/bootstrap.css" "css/todo-custom.css" ))


# Database
(class +Todo +Entity)
(rel dat (+Ref +Date))                 # Date
(rel item (+IdxFold +String))          # Item
(rel color (+String))                  # Color

(de addToDatabase (Item Date Color)
   (new! '(+Todo)
      'item Item
      'dat Date
      'color Color ) )


(de todoList ()
   (and (app) *Port% (redirect (baseHRef) *SesId *Url))
   (action
      (html 0 "Todo" *Css "todo-body"
         # Header
         (<div> "bg-white container-sm mt-2"
            (<div> "d-flex flex-column bg-white"
               (<h1> "d-flex mt-4 m-2 justify-content-center" "Things To Do") )
            (<hr>)
            # prevents that the server closes down
            (<ping> 2)
            (form NIL
               #  add new items
               (<div> "container-sm"
                  (<div> "row py-3 bg-light mx-1"
                     (<div> "col-auto p-3"
                        (gui '(+Cue +TextField) "Thing to do" 30 ) )
                     (<div> "col-auto p-3"
                        (gui '(+Cue +DateField) "Enter a date" 10 ) )
                     (<div> "col p-3"
                        (<span> NIL "Color: ")
                        (gui '(+TextField) '("black" "red" "green" "yellow" "blue")) )
                     (<div> "col-auto p-3"
                        (gui '(+Style +Button) "button-icon" T "img/plus.png"
                           '(prog
                              (addToDatabase (val> (field -3)) (val> (field -2)) (val> (field -1)))
                              (init> (: home query)) ) ) ) ) )
               #  search all
               (gui 'query '(+QueryChart) 4
                  '(goal
                     (quote
                        @Dat (cons NIL T)
                        @Item ""
                        (select (@@)
                           ((dat +Todo @Dat) (item +Todo @Item))
                           (range @Dat @@ dat)
                           (part @Item @@ item) ) ) )
                  3
                  '((This)
                     (list (: dat) (: item) (: color)) ) )
               #  display
               (do 4
                  (<div> "container-sm"
                     (<div> "row pt-3 border m-1"
                        (<div> "col-auto "
                           (gui 1 '(+DateField)) )
                        (<div> "col text-align-left "
                           (gui 2 '(+Style +TextField)
                              '(pack  (curr 'color) " text-capitalize font-weight-bold col") ) )
                        (<div> "col-auto "
                           (gui 3 '(+Style +JS +Able +Button)
                              "button-icon mb-3"
                              '(nth (: chart 1 data) (row))
                              T "img/delete.png"
                              '(prog
                                 (lose!> (curr))
                                 (init> (: home query)) ) ) ) ) ) ) 
            (<div> "d-flex justify-content-center mx-5"
               (scroll 4) ) ) )
         # Footer
         (<div> "d-flex flex-column m-5 text-white"
            (<p> NIL "View source code on "
               (<href> "Gitlab" "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/todo-list-example/todo-responsive.l" "_blank") ) ) ) ) ) 

(de main ()
   (pool "db/todo-responsive.db") )

(de go ()
   (server 8086 "!todoList") )
