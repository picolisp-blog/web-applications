(allowed ()
   "!work" "@lib.css" )

(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l")

(class +Todo +Entity)
(rel dat (+Ref +Date))                 # Date
(rel item (+IdxFold +String))          # Item

(de work ()
   (and (app) *Port% (redirect (baseHRef) *SesId *Url))
   (action
      (html 0 Ttl "@lib.css" NIL
         (<ping> 2)
         (form NIL
            (<grid> "--."
               "Date" (gui 'dat '(+DateField) 10)
               (searchButton '(init> (: home query)))
               "Item" (gui 'item '(+DbHint +TextField) '(item +Todo) 30)
               (resetButton '(dat item query)) )
            (gui 'query '(+QueryChart) 12
               '(goal
                  (quote
                     @Dat (cons (or (val> (: home dat)) T))
                     @Item (val> (: home item))
                     (select (@@)
                        ((dat +Todo @Dat) (item +Todo @Item))
                        (range @Dat @@ dat)
                        (part @Item @@ item) ) ) )
               3
               '((This) (list (: dat) (: item)))
               '((L D)
                  (cond
                     (D
                        (put!> D 'dat (car L))
                        (put!> D 'item (cadr L))
                        D )
                     ((car L)
                        (new! '(+Todo) 'dat (car L)) ) ) ) )
            (<table> NIL (choTtl "Entries" '+Todo)
               '((NIL "Date") (NIL "Item"))
               (do 12
                  (<row> NIL
                     (gui 1 '(+DateField) 10)
                     (gui 2 '(+TextField) 40)
                     (gui 3 '(+DelRowButton)
                        '(lose!> (curr))
                        '(text "Delete Item from @1?" (datStr (curr 'dat))) ) ) ) )
            (scroll 12) ) ) ) )

(de main ()
   (pool "db/todo.db") )

(de go ()
   (server 8086 "!work") )
