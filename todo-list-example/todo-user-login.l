(allowed ("css/" "img/")
   "!todoList" "@lib.css" )

(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l" "@lib/adm.l")

(setq *Salt (16 . "$6$@1$"))


(setq *Css '("@lib.css" "css/bootstrap.css" "css/todo-custom.css" ))


# Database
(class +Todo +Entity)
(rel dat (+Ref +Date))                 # Date
(rel item (+IdxFold +String))          # Item
(rel color (+String))                  # Color

(de addToDatabase (Item Date Color)
   (new! '(+Todo)
      'item Item
      'dat Date
      'color Color ) )


(de todoList ()
   (and (app) *Port% (redirect (baseHRef) *SesId *Url))
   (action
      (html 0 "Todo" *Css "todo-body"
         # prevents that the server closes down
         (<ping> 2)
         # Header
         (<div> "bg-white container-sm my-4"
            (ifn *Login
               (form NIL
                  (<div> "container-sm"
                     (<h3> "mt-5" "Welcome to the PicoLisp ToDo-App!")
                     (<p> "my-3" "Please enter username and password: ")
                     (<div> "row py-3 bg-light mx-1"
                        (<div> "col-auto p-3"
                           (<span> NIL "Username: " )
                           (gui 'nm '(+Focus +TextField) 20) )
                        (<div> "col-auto p-3"
                           (<span> NIL "Password: ")
                           (gui 'pw '(+PwField) 20) )
                        (gui '(+Button) ,"Login"
                           '(ifn (login (val> (: home nm)) (val> (: home pw)))
                              (error ,"Permission denied")
                              (url "!todoList")  ) ) ) ) )
               (<div> "d-flex flex-column bg-white"
                  (form NIL
                     (<div> "d-flex justify-content-end mt-3"
                        (gui '(+Style +Button)
                           "button-icon m-1"
                           T "img/logout.png"
                           '(prog
                              (post (logout)) ) ) ) )
                  (<h1> "d-flex m-2 justify-content-center" "Things To Do")
                  (<hr>)
                  (form NIL
                     #  add new items
                     (<div> "container-sm"
                        (<div> "row py-3 bg-light mx-1"
                           (<div> "col-auto p-3"
                              (gui '(+Cue +TextField) "Thing to do" 30 ) )
                           (<div> "col-auto p-3"
                              (gui '(+Cue +DateField) "Enter a date" 10 ) )
                           (<div> "col p-3"
                              (<span> NIL "Color: ")
                              (gui '(+TextField) '("black" "red" "green" "yellow" "blue")) )
                           (<div> "col-auto p-3"
                              (gui '(+Style +Button) "button-icon" T "img/plus.png"
                                 '(prog
                                    (addToDatabase (val> (field -3)) (val> (field -2)) (val> (field -1)))
                                    (init> (: home query)) ) ) ) ) )
                     #  search all
                     (gui 'query '(+QueryChart) 4
                        '(goal
                           (quote
                              @Dat (cons NIL T)
                              @Item ""
                              (select (@@)
                                 ((dat +Todo @Dat) (item +Todo @Item))
                                 (range @Dat @@ dat)
                                 (part @Item @@ item) ) ) )
                        3
                        '((This)
                           (list (: dat) (: item) (: color)) ) )
                     #  display
                     (do 4
                        (<div> "container-sm"
                           (<div> "row pt-3 border m-1"
                              (<div> "col-auto "
                                 (gui 1 '(+DateField)) )
                              (<div> "col text-align-left "
                                 (gui 2 '(+Style +TextField)
                                    '(pack  (curr 'color) " text-capitalize font-weight-bold col") ) )
                              (<div> "col-auto "
                                 (gui 3 '(+Style +JS +Able +Button)
                                    "button-icon mb-3"
                                    '(nth (: chart 1 data) (row))
                                    T "img/delete.png"
                                    '(prog
                                       (lose!> (curr))
                                       (init> (: home query)) ) ) ) ) ) )
                     (<div> "d-flex justify-content-center mx-5"
                        (scroll 4) )) ) ) )
         # Footer
         (<div> "container-sm"
            (<div> "d-flex flex-column m-5 text-white"
               (<p> NIL "View source code on "
                  (<href> "Gitlab" "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/todo-list-example/todo-user-login.l" "_blank") ) ) ) ) ) )

(de main ()
   (pool "db/todo-responsive.db") )

(de go ()
   (server 8086 "!todoList") )
