Corresponding posts:

- ``todo-desktop.l``: [How to create a To-Do App in PicoLisp (Desktop Version)](https://picolisp-explored/how-to-create-a-to-do-app-in-picolisp-desktop-version)
- ``todo-responsive-noCSS.l``: [How to create a To-Do App in PicoLisp (Responsive Version) - Part 1](https://picolisp-explored/how-to-create-a-to-do-app-in-picolisp-responsive-version-part-1)
- ``todo-responsive.l``: [How to create a To-Do App in PicoLisp (Responsive Version) - Part 2](https://picolisp-explored/how-to-create-a-to-do-app-in-picolisp-responsive-version-part-2)
- ``todo-desktop-with-database.l``: [Creating a Todo App - 3: Adding the Database](https://picolisp-explored.com/creating-a-todo-app-3-adding-the-database)
- ``todo-responsive-with-database.l``: [Creating a Todo App - 4: Customizing the Database Output](https://picolisp-explored.com/creating-a-todo-app-4-customizing-the-database-output)
- ``todo-user-login-rolebased.l``, ``todo-user-login.l``: [Creating a Todo App - 5: Adding the User Login](https://picolisp-explored.com/creating-a-todo-app-5-adding-the-user-login)
- ``todo-multi-language.l``: [Creating a Todo App - 6: Multi-Language Support](https://picolisp-explored.com/creating-a-todo-app-6-multi-language-support)
- ``todo-rest.l``: [Creating a Todo App - 7: Database Manipulation via API](https://picolisp-explored.com/creating-a-todo-app-7-database-manipulation-via-api)
- ``names``: [Creating a Todo App - 8: Going into Production with the httpGate Proxy Server](https://picolisp-explored.com/creating-a-todo-app-8-going-into-production-with-the-httpgate-proxy-server)

