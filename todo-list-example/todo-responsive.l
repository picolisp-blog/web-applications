(setq *Css '("@lib.css" "css/bootstrap.css" "css/todo-custom.css" ))

(de addToList ( Item Date Color )
   (queue '*ToDoList (list Item Date Color)) )  


(and (app) *Port% (redirect (baseHRef) *SesId *Url))

(action
   (html 0 "Todo" *Css "todo-body"
      (<div> "bg-white container-sm mt-2"
         (<div> "d-flex flex-column bg-white"
            (<h1> "d-flex mt-4 m-2 justify-content-center" "Things To Do") )
         (<hr>)
         (<div> "container-sm"
            (form NIL
               (for (I . @Item) *ToDoList
                  (let Color (caddr @Item)
                     (<div> "row pt-3 border m-1"
                        (<div> "col-auto "
                           (<p> NIL (prinl I ".")) )
                        (<div> "col-auto text-align-left"
                           (<p> NIL (prinl (datStr (cadr @Item)) ) ) )
                        (<div> (list "col" (cons 'style (pack "color: " Color)) )
                           (<p> "text-capitalize font-weight-bold" (prinl (car @Item))) )
                        (<div> "col-auto"
                           (gui '(+Style +Button) "button-icon" T "img/delete.png"
                              (fill
                                 '(post
                                    (del '@Item '*ToDoList) ) ) ) ) ) ) ) ) )
         (<div> "container-sm"
            (form NIL
               (<div> "row py-3 bg-light mx-1"
                  (<div> "col-auto p-3"
                     (gui '(+Cue +TextField) "Thing to do" 30 ) )
                  (<div> "col-auto p-3"
                     (gui '(+Cue +DateField) "Enter a date" 10 ) )
                  (<div> "col p-3"
                     (<span> NIL "Color: ")
                     (gui '(+RgbPicker)) )
                  (<div> "col-auto p-3 "
                     (gui '(+Style +Button) "button-icon" T "img/plus.png"
                        '(post
                           (addToList (val> (field -3)) (val> (field -2)) (val> (field -1))) ) ) ) ) ) ) )
      (<div> "container-sm mt-3 text-white"
         (<p> NIL "View source code on "
            (<href> "Gitlab" "https://gitlab.com/picolisp-blog/web-applications/-/blob/main/todo-list-example/todo-responsive.l" "_blank") ) ) ) )

