(setq *Css '("@lib.css" ))

(ifn *ToDoList
   (setq *ToDoList '(("shopping" 781423 "green")("cooking" 732842 "red"))) )

(de addToList ( Item Date Color )
   (queue '*ToDoList (list Item Date Color )) )

(and (app) *Port% (redirect (baseHRef) *SesId *Url))

(action
   (html 0 "Todo" *Css NIL

      (<h1> "" "Things To Do")

      (form NIL
         (for (I . @Item) *ToDoList
            (<p> NIL (printsp @Item))
            (gui '(+Button) "Delete"
               (fill
                  '(post
                     (del '@Item '*ToDoList) ) ) ) ) )

      (form NIL
         (gui '(+Cue +TextField) "Thing to do" 30)
         (gui '(+Cue +DateField) "Enter a date" 10)
         (gui '(+RgbPicker) )
         (gui '(+Button) "add item"
            '(post
               (addToList (val> (field -3)) (val> (field -2)) (val> (field -1))) ) ) ) ) )


