(setq *Css '("@lib.css" "css/bootstrap.css"))

(html 0 "Hello World - CSS Bootstrap" *Css NIL 
   (<div> "d-flex flex-column"
      (<h1> "d-flex m-5 justify-content-center" "Hello World!") 
      (<h2> "d-flex m-3 justify-content-center" "Playing around with Bootstrap classes.") ) 
   (<hr>)
   (<p> "text-white bg-dark m-4 d-inline-flex p-1"
      "This is a text block. "
      (<strong> " Still the same text block. ") 
      " Still a text block.")
   (<hr>)
   (<h5> "ml-5" "This is the bootstrap grid system:")
   (<div> "row mx-3 bg-secondary"
      (<div> "col border rounded m-1 p-3 text-success bg-white" "This is a div in a div."
      (<ul> "text-dark"
         (<li> NIL "Item 1")
         (<li> NIL "Item 2"
         (<ul> "text-secondary"
            (<li> NIL "2-1")
            (<li> NIL "2-2") ) ) ) ) 
      (<div> "col border rounded m-1 p-3 text-success bg-white" "This is a second div in a div."
      (<ul> "text-dark"
         (<li> NIL "Item 3")
         (<li> NIL "Item 4"
         (<ul> "text-secondary"
            (<li> NIL "4-1")
            (<li> NIL "4-2") 
            (<li> "border rounded m-2 text-danger bg-light" "div in a div in a div") ) ) ) ) ) 
   (<div> "row mx-3 mt-1 bg-secondary" 
      (<div> "col border rounded m-1 p-3 bg-white text-center"
         (<img> "img/Sunflower_from_Silesia.JPG" "Sunflower" NIL 300 300) ) ) 

  (<div> "d-flex flex-column m-5"
      (<p> NIL "View source code on "
         (<href> "Gitlab" "https://gitlab.com/picolisp-blog/web-applications/-/blob/master/bootstrap-example/helloworld-bootstrapped.l" "_blank") ) ) ) 
