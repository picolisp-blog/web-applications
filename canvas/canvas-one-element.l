(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l" "@lib/canvas.l")

(class +Rectangle)
# x y dx dy stroke fill

(dm draw> ()
   (csRect (: x) (: y) (: dx) (: dy))
   (csStrokeStyle (: stroke))
   (csFillStyle (: fill)) )

(object 'rectangle '(+Rectangle)
   'x 100
   'y 100
   'dx 200
   'dy 100
   'stroke "blue"
   'fill "red" )


(de drawCanvas (Id Dly)
   (make
      (draw> Id)
      (csStroke)
      (csFill) ) )

(de canvasTest ()
   (and (app) *Port% (redirect (baseHRef) *SesId *Url))
   (action
      (html 0 "Canvas Test" "@lib.css" NIL
         #  Rectangle
         (<h1> NIL "This is a rectangle")
         (<canvas> "$rectangle" 400 300)
         (javascript NIL "onload=drawCanvas('$rectangle', -1)") ) ) )

(server 8086 "!canvasTest")

