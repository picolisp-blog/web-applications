Corresponding posts:

- ``canvas-basic.l``, ``canvas-one-element.l``, ``canvas-two-elements.l``: [The PicoLisp Canvas Library](https://picolisp-explored.com/the-picolisp-canvas-library)
- ``zappel.l``, ``zappel-single-step.l``: [Creating dynamic graphs with Canvas](https://picolisp-explored.com/creating-dynamic-graphs-with-canvas)

