(load "@lib/http.l" "@lib/xhtml.l" "@lib/form.l" "@lib/canvas.l")
(setq *Css '("@lib.css" "css/bootstrap.css"))

(class +Rectangle)
# x y dx dy stroke fill

(dm draw> ()
   (csRect (: x) (: y) (: dx) (: dy))
   (csStrokeStyle (: stroke))
   (csFillStyle (: fill)) )

(object 'rectangle '(+Rectangle)
   'x 100
   'y 100
   'dx 200
   'dy 100
   'stroke "blue"
   'fill "red" )


(class +Circle)
# x y r a b stroke fill

(dm draw> ()
   (csArc (: x) (: y) (: r) (: a) (: b))
   (csStrokeStyle (: stroke))
   (csFillStyle (: fill)) )

(object 'circle '(+Circle)
   'x 200
   'y 150
   'r 75
   'a 0
   'b "6.2832"
   'stroke "green"
   'fill "blue" )


(de drawCanvas (Id Dly)
   (make
      (draw> Id)
      (csStrokeRect 0 0 400 300)
      (csFill) ) )

(de canvasTest ()
   (and (app) *Port% (redirect (baseHRef) *SesId *Url))
   (action
      (html 0 "Canvas Test" *Css NIL
         #  Rectangle
         (<div> "container-sm"
         (<h1> "m-3" "This is a rectangle")
         (<canvas> "$rectangle" 400 300)
         (javascript NIL "onload=drawCanvas('$rectangle', -1)")
         # Circle
         (<h1> "m-3" "This is a circle")
         (<canvas> "$circle" 400 300)
         (javascript NIL "onload=drawCanvas('$circle', -1)") ) ) ) )

(server 8086 "!canvasTest")
